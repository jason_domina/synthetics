# README #

This will guide you to setting up and viewing our New Relic Synthetic scripted browser tests

### What is this repository for? ###

* This is a repo for tracking and developing our New Relic Synthetics scripted browser scripts.  The monitors that we run can [be viewed here:](https://synthetics.newrelic.com/accounts/103865/monitors) 


### How do I get set up? ###

* To get started, see [this article for ](https://stelladot.jira.com/wiki/spaces/QUAL/pages/191238697/How+to+setup+a+local+testing+environment+for+New+Relic+Synthetics) details: , but in a nutshell here's what you'll need to do

1. Install node.js
1. Install synthmanager
1. Generate a personal Admin API Key in New Relic
1. Import an existing Synthetics test from New Relic
1. Install homebrew
1. Use homebrew to install Selenium Server Standalone
1. Install JDK 8u121 (the latest at the time of this writing)
1. Install ChromeDriver
1. TEST LOCALLY!  YAY!

### Contribution guidelines ###

* Eventually, we'll cover guidelines here but the main guideline - we only have coverage for about 15 tests - don't needlessly add tests to our production environment

### Who do I talk to? ###

* Jason Domina <jdomina@stelladot.com>