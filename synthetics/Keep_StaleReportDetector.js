require('new-relic-synthetics-manager');
//----- Add synthetic code below this line


/*
 this is the time we'll compare to down below.  Its a number in milliseconds, just written out in a way that 
 makes a 3am false alarm easier to manage.  IE change the 20 (for 20 minutes) to 30 or 40 if you want to push 
 the delay out that long
*/
var reportTooOldMinutes = 1000 * 60 * 35


// setting live URL here...
var homePageURL = 'https://lounge.keepcollective.com';
// however, we can pass in a different URL to test against any other environment
//if(process.argv.indexOf("-u") != -1){homePageURL = process.argv[process.argv.indexOf("-u") + 1]}

var assert = require('assert');
var browserInitialPageLoadInMS = 500;
var browserPageLoadInMS = 3000;
var browserNemusLoadInMS = 20000;
var browserElementLoadInMS = 500;
var userName = '201681457';
var password = 'testdsr';
var userFieldCSSPath = '#login-field';
var passwordFieldCSSPath = '#password-field'
var titleSpanCSS = '#signin-form > h1';
var foundTheHomePage = 'Designer Sign In';
var signInButtonCSSPath = '#signin_submit';
var reportMenuCSSPath = '#main-nav > ul > li:nth-child(5) > a';
var businessDashCSSPath = '#main-nav > ul > li:nth-child(5) > ul > li:nth-child(1) > a';
var businessDashText = 'My Business Dash'
var reportDateCSSPath = '#report-header-last-updated';
var reportDateXPath = '//*[@id="report-header-last-updated"]';



var assert = require('assert'),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  lastMsg = '',
  browser = $browser.manage(),
  By = $driver.By,
  thisStep = 0,
  VARS = {};

// enhanced logging with timestamps.  This also pushes the timestamps into Insights
var log = function(msg) {
   if (thisStep > 0) {
     var lastStep = thisStep - 1;
     var lastStepTimeElapsed = Date.now() - (startTime + stepStartTime);
     console.log('Step ' + lastStep + ': ' + lastMsg + ' FINISHED. It took ' + lastStepTimeElapsed + 'ms to complete.');
     //if(process.argv[2] == '-l'){
      //we're running in local mode and don't have access to the insights toolset;
      //} else {
      //  $util.insights.set('Keep_A2C_Step ' + lastStep + ': ' + lastMsg, lastStepTimeElapsed);
      //}
   }
   stepStartTime = Date.now() - startTime;
   console.log('Step ' + thisStep + ': ' + msg + ' STARTED at ' + stepStartTime + 'ms.');
   lastMsg = msg;
   ++thisStep;
 };


$browser.get(homePageURL).then(function(){
  log('going to the lounge sign in Page - Page Load 1')
  return $browser.sleep(browserInitialPageLoadInMS).then(function(){
      return $browser.findElement($driver.By.css(titleSpanCSS)).then(function(element){
        return element.getText().then(function(text){
        log('asserting that we found evidence that we made it')
        assert.equal(foundTheHomePage, text, 'Lounge sign in header did not match' + text);
      })
    })
  }) 
}).then(function(){
  log("entering username");
  return $browser.findElement($driver.By.css(userFieldCSSPath)).sendKeys(userName).then(function(){
    return $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  log("entering password");
  return $browser.findElement($driver.By.css(passwordFieldCSSPath)).sendKeys(password).then(function(){
    $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  log("submitting form - Page Load");
  return $browser.findElement($driver.By.css(signInButtonCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("mousing over Reports")
  return $browser.findElement($driver.By.css(reportMenuCSSPath)).then(function(element){
    return $browser.actions().mouseMove(element).perform().then(function(){
      log("clicking the My Business Dashboard Link - Page Load 1")
      return $browser.findElement($driver.By.partialLinkText(businessDashText)).click().then(function(){
        $browser.sleep(browserPageLoadInMS)
      })
    })
  })
}).then(function(){
  $browser.getAllWindowHandles().then(function(windowHandlers) {
	$browser.switchTo().frame("report-iframe").then(function() {
  		log("finding last updated text");
  		return $browser.sleep(browserNemusLoadInMS).then(function(){
  	  		return $browser.findElement($driver.By.css(reportDateCSSPath)).then(function(element){
  	  			return element.getText().then(function(text){
  	  				var now = Date.now();
  	  				var dateString = String(text).slice(8);
  	  				dateString = String(dateString).replace(/ PM| AM/, "")
  	  				var updatedDate = Date.parse(dateString)
  	  				//ugly hack - when run on the NR servers, the date of Date.now is in UTC.  The date posted on the report page
  	  				// is in US Pacific Timezone.  So, if running locally, I'm just going to use what I get here in the PDT, but 
  	  				// otherwise, I'm going to add 7 hours to now, hope to hell I've got something better in place before we change
  	  				// to PST on Sunday, November 5th, 2017
  	  				//if(process.argv[2] != '-l'){
  	  					updatedDate += (1000 * 60 * 60 * 7) // adding 7 hours because I know the report date is always in PDT
  	  				//}
  	  				var elapsedTime = (now - updatedDate)
  	  				log('elapsed time was ' + elapsedTime / 1000 / 60 + " minutes")
  	  				assert.ok(reportTooOldMinutes > elapsedTime, "The report was too long ago.  It was updated " + elapsedTime / 1000 / 60 + " minutes ago which is greater than " + reportTooOldMinutes / 1000 / 60 + " minutes.  If this is false alarm, change the reportTooOldMinutes value (in milliseconds) in the script to something longer than currently configured")
  	  			})
  			})
  		})
  	})
  })
}).then(function(){
  log("closing browser now")
  $browser.close()
})

