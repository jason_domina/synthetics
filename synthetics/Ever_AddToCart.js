require('new-relic-synthetics-manager');
//----- Add synthetic code below this line
var assert = require('assert');
var browserPageLoadInMS = 3000;
var browserElementLoadInMS = 1000;
var browserInitialPageLoadInMS = 100;
var homePageURL = 'http://www.everskin.com';

var homePageURL = 'http://www.everskin.com';
// however, we can pass in a different URL to test against any other environment
//if(process.argv.indexOf("-u") != -1){homePageURL = process.argv[process.argv.indexOf("-u") + 1]}

var foundTheHomePage = 'SHOP';
var letsNotKeepInTouchXPath = '//*[@id="justuno_form"]/div/div[2]/div[9]/div/div/div/span/span/span';
var shopLinkCSSPath = '#main-nav-bar > ul > li:nth-child(1) > a';
var stepOneNavCSSPath = '#categories > div > div.left-navigation-wrap__desktop.ng-isolate-scope.affix > ul > li.category-tab.ng-scope.ng-isolate-scope.root.category-tab--active > a';
var stepOneText ='CLEANSERS';
var luminousBalmXPath= '//*[@id="81-step-1-cleanse"]/product-list/div/div[1]/div/a/figure/div';
var luminousBalmText = 'LUMINOUS'
var addToBagXPath = '//*[@id="content"]/pdp/div/div/div[2]/div/div[1]/div[2]/div/button';
var viewBagCSSPath = '#main-nav-bar > div > span.ever-nav-bag-and-search__bag > a'
var bagCountCSSPath = '#main-nav-bar > div > span.ever-nav-bag-and-search__bag > a > span.ever-nav-bag-and-search__bag-count > span'
var checkoutButtonXPath = '//*[@id="content"]/div/div/div[1]/div/div[2]/button';
var userEmail = 'synthetics@stelladot.com';
var password = '111111';
var userEmailXPath = '//*[@id="content"]/signin-page/div/div[1]/div/form/form-control[1]/div/span[1]/input';
var passwordXPath = '//*[@id="content"]/signin-page/div/div[1]/div/form/form-control[2]/div/span[1]/div/input';
var signinButtonXPath = '//*[@id="content"]/signin-page/div/div[1]/div/form/p[3]/button';
var continueCheckoutButtonCSSPath = 'body > div.modal.fade.ng-isolate-scope.in > div > div > dsr-order-confirm-modal > div > div.modal-footer > button.btn.btn-primary.btn-transparent';
var checkoutBannerCSSPath = '#checkout-layout > h1' 
var clearOrderXPath = '//*[@id="cart"]/div/div[3]/div[2]/a';
var clearOrderCSSPath = '#cart > div > div.row.overpass.small > div.col-xs-6.text-left > a'
var clearOrderText = 'Clear Order'
var modalYesButtonXPath = '/html/body/div[6]/div/div/div[3]/button[1]';
var modalYesButtonCSSPath = 'body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-footer.ng-scope > button.btn.btn-primary'
var accountLinkCSSPath = '.navbar-connect__signin-dropdown'
var signOutLinkCSSPath = ".navbar-connect__signin-dropdown a[href='/signout']"
var taxCalculatedCSSPath = '#tax';


var assert = require('assert'),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  lastMsg = '',
  browser = $browser.manage(),
  By = $driver.By,
  thisStep = 0
  VARS = {};

// enhanced logging with timestamps.  This also pushes the timestamps into Insights
var log = function(msg) {
   if (thisStep > 0) {
     var lastStep = thisStep - 1;
     var lastStepTimeElapsed = Date.now() - (startTime + stepStartTime);
     console.log('Step ' + lastStep + ': ' + lastMsg + ' FINISHED. It took ' + lastStepTimeElapsed + 'ms to complete.');
     /*
     if(process.argv[2] == '-l'){
      //we're running in local mode and don't have access to the insights toolset;
      } else {
        $util.insights.set('Ever_A2C_Step ' + lastStep + ': ' + lastMsg, lastStepTimeElapsed);
      }
      */
   }
   stepStartTime = Date.now() - startTime;
   console.log('Step ' + thisStep + ': ' + msg + ' STARTED at ' + stepStartTime + 'ms.');
   lastMsg = msg;
   ++thisStep;
 };



$browser.get(homePageURL).then(function(){
  log("trying to close pop-up on home page")
  return $browser.sleep(browserPageLoadInMS).then(function(){
    $browser.getAllWindowHandles().then(function(windowHandlers) {
      var framePresent = "Yes";
      $browser.switchTo().frame("ju_iframe").then(null,function (err) {
        framePresent = "No";
        log('err.name = ' + err.name)
      }).then(function(){
        log("If there was a frame present, we should try and close it: " + framePresent)
        if (framePresent === "Yes"){
          return $browser.sleep(browserPageLoadInMS).then(function(){
           return $browser.findElement($driver.By.xpath(letsNotKeepInTouchXPath)).click().then(function(){
              return $browser.switchTo().defaultContent();
            })
          }) 
        }
      })
    })
  })
}).then(function(){
  log("top nav click the shop link - Page Load");
  return $browser.sleep(browserPageLoadInMS).then(function(){
    return $browser.findElement($driver.By.css(shopLinkCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  })
}).then(function(){
    log("left nav click step one on the nav bar from the shop page");
    return $browser.findElement($driver.By.partialLinkText(stepOneText)).click().then(function(){
      return $browser.sleep(browserElementLoadInMS)
    })
}).then(function(){
  log("click on the Luminous Balm from the shop page - Page Load");
  return $browser.findElement($driver.By.partialLinkText(luminousBalmText)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS) 
  })
}).then(function(){
  log("adding to bag - Page Load")
  return $browser.findElement($driver.By.xpath(addToBagXPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  return $browser.findElement($driver.By.css(bagCountCSSPath)).then(function(element){
    return element.getText().then(function(text){
      log('asserting that we have something visibly in the bag')
      assert.equal("1", text, 'There was not one item in the bag: ' + text)
    })
  })
}).then(function(){
  log("viewing bag - Page Load");
  return $browser.findElement($driver.By.css(viewBagCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("clicking checkout button - Page Load");
  return $browser.findElement($driver.By.xpath(checkoutButtonXPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("entering username");
  return $browser.findElement($driver.By.xpath(userEmailXPath)).sendKeys(userEmail).then(function(){
    return $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  log("entering password");
  return $browser.findElement($driver.By.xpath(passwordXPath)).sendKeys(password).then(function(){
    $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  log("submitting form - Page Load");
  return $browser.findElement($driver.By.xpath(signinButtonXPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS + 5000)
  })
}).then(function(){
  log("Continuing Checkout - Page Load")
  return $browser.sleep(browserPageLoadInMS).then(function(){
    return $browser.switchTo().activeElement().then(function(){
      return $browser.findElement($driver.By.css(continueCheckoutButtonCSSPath)).click().then(function(){
        return $browser.sleep(browserPageLoadInMS)
      })
    })
  })
}).then(function(){
  return $browser.findElement($driver.By.css(checkoutBannerCSSPath)).then(function(element){
    return element.getText().then(function(text){
      log('asserting that we are actually on the checkout page')
      assert.equal("CHECKOUT", text, 'NOPE.  Not on the checkout page: ' + text)
    }).then(function(){
      return $browser.waitForAndFindElement($driver.By.css(taxCalculatedCSSPath), 60000).then(function(element){
        return element.getText().then(function(text){
          log('Waiting to get Tax Calculation.  If this times out, there may be a problem with Avalara')
          assert.equal("$4.06", text, 'NOPE.  Did not get right tax amount: ' + text)
        })
      })
    })
  })
}).then(function(){
  log("clearing order now")
  return $browser.sleep(browserPageLoadInMS).then(function(){
    return $browser.switchTo().defaultContent().then(function(){
      return $browser.findElement($driver.By.partialLinkText(clearOrderText)).click().then(function(){
        return $browser.sleep(browserElementLoadInMS)
      })
    })  
  })  
}).then(function(){
  log("confirming order clearing intention - Page Load")
  return $browser.sleep(browserPageLoadInMS).then(function(){
    return $browser.switchTo().activeElement().then(function(){
      return $browser.findElement($driver.By.css(modalYesButtonCSSPath)).click().then(function(){
        return $browser.sleep(browserPageLoadInMS)
      })
    })
  })
}).then(function(){
  log("We should sign out now")
  return $browser.findElement($driver.By.css(accountLinkCSSPath)).then(function(element){
    $browser.sleep(browserElementLoadInMS)
    log("mousing over 'Hi Carrie!")
    return $browser.actions().mouseMove(element).perform().then(function(element){
      return $browser.sleep(browserElementLoadInMS)
    })
  })
}).then(function(){
  log("finding the sign out link and clicking it - Page Load")
  return $browser.findElement($driver.By.css(signOutLinkCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS + 500)
   })
}).then(function(){
  log("closing browser now")
  $browser.close()
})

