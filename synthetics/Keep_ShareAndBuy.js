require('new-relic-synthetics-manager');
//----- Add synthetic code below this line


// setting live URL here...
var homePageURL = 'https://lounge.keepcollective.com';
// however, we can pass in a different URL to test against any other environment
//if(process.argv.indexOf("-u") != -1){homePageURL = process.argv[process.argv.indexOf("-u") + 1]}

var assert = require('assert');
var browserInitialPageLoadInMS = 500;
var browserPageLoadInMS = 3000;
var browserElementLoadInMS = 500;
var userName = '201681457';
var password = 'testdsr';
var userFieldCSSPath = '#login-field';
var passwordFieldCSSPath = '#password-field'
var titleSpanCSS = '#signin-form > h1';
var foundTheHomePage = 'Designer Sign In';
var signInButtonCSSPath = '#signin_submit';
var nonDesignSessionOrderCSSPath = '#non-charm-order';
var createOrderText = 'Place An Order'
var placeAnOrderCSSPath = '#sales_lounge_card > div > div.lounge-card-link > a'
var createOrderButtonCSSPath = '#btn-customer-order'
var searchContactsText = 'Search contacts'
var contactSearchFieldCSSPath = '#box > div > div > input';
var contactSearchTerm = 'Test';
var testCustomerCSSPath = '#search-result > table > tbody > tr:nth-child(1) > td > div > div.col-md-3.col-xs-8';
var productSearchCSSPath = '#box > div > div > input';
var productSearchText = 'single woven';
var firstItemCSSPath = '#search_result > div > table > tbody > tr:nth-child(1) > div > div.visible-xs > div.clearfix.text-right > button';
var firstItemXPath = '//*[@id="search_result"]/div/table/tbody/tr[1]/div/div[2]/div[2]/div/div[4]/button';
var shoppingBagXPath = '//*[@id="view_cart"]';
var viewBagText = 'VIEW BAG / CHECKOUT';
var cartURL = homePageURL + '/orders/cart';
var shareBagCSSPath = '#share_cart';
var linkToShareCSSPath = '#content-body > a'
var closeShareBagXCSSPath = '#generic-modal-container > div > div > div > div.modal-header > button';
var emptyCartCSSPath = '#content > div > div > table > tbody > tr > td:nth-child(1) > div > div.cart-item-desc > a';




var assert = require('assert'),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  lastMsg = '',
  browser = $browser.manage(),
  By = $driver.By,
  thisStep = 0,
  VARS = {};

// enhanced logging with timestamps.  This also pushes the timestamps into Insights
var log = function(msg) {
   if (thisStep > 0) {
     var lastStep = thisStep - 1;
     var lastStepTimeElapsed = Date.now() - (startTime + stepStartTime);
     console.log('Step ' + lastStep + ': ' + lastMsg + ' FINISHED. It took ' + lastStepTimeElapsed + 'ms to complete.');
     /*
     if(process.argv[2] == '-l'){
      //we're running in local mode and don't have access to the insights toolset;
      } else {
        $util.insights.set('Keep_A2C_Step ' + lastStep + ': ' + lastMsg, lastStepTimeElapsed);
      }
      */
   }
   stepStartTime = Date.now() - startTime;
   console.log('Step ' + thisStep + ': ' + msg + ' STARTED at ' + stepStartTime + 'ms.');
   lastMsg = msg;
   ++thisStep;
 };

$browser.get(homePageURL).then(function(){
  log('going to the lounge sign in Page - Page Load 1')
  return $browser.sleep(browserInitialPageLoadInMS).then(function(){
      return $browser.findElement($driver.By.css(titleSpanCSS)).then(function(element){
        return element.getText().then(function(text){
        log('asserting that we found evidence that we made it')
        assert.equal(foundTheHomePage, text, 'Lounge sign in header did not match' + text);
      })
    })
  }) 
}).then(function(){
  log("entering username");
  return $browser.findElement($driver.By.css(userFieldCSSPath)).sendKeys(userName).then(function(){
    return $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  log("entering password");
  return $browser.findElement($driver.By.css(passwordFieldCSSPath)).sendKeys(password).then(function(){
    $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  log("submitting form - Page Load");
  return $browser.findElement($driver.By.css(signInButtonCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("clicking Place an order button - Page Load");
  return $browser.switchTo().frame("nemusIframe").then(function(){
  	return $browser.waitForAndFindElement($driver.By.css(placeAnOrderCSSPath), 15000).then(function(){
  		return $browser.findElement($driver.By.css(placeAnOrderCSSPath)).click().then(function(){
    		return $browser.sleep(browserPageLoadInMS)
  		})
  	})
  })
}).then(function(){
  log("clicking create order button - Page Load");
  return $browser.getAllWindowHandles().then(function(windowHandles){
  	$browser.switchTo().window(windowHandles[1]).then(function(){
  		return $browser.sleep(browserPageLoadInMS).then(function(){
  			return $browser.findElement($driver.By.css(createOrderButtonCSSPath)).click().then(function(){
    			return $browser.sleep(browserPageLoadInMS)
  			})
  		})
  	})
  })
}).then(function(){
  log("clicking create a non-design session order - Page Load");
  return $browser.findElement($driver.By.css(nonDesignSessionOrderCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("clicking search contacts - Page Load");
  return $browser.findElement($driver.By.partialLinkText(searchContactsText)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("searching for Test");
  return $browser.findElement($driver.By.css(contactSearchFieldCSSPath)).sendKeys(contactSearchTerm).then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("clicking TestKeep Customer - Page Load");
  return $browser.findElement($driver.By.css(testCustomerCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("searching for single woven band");
  return $browser.findElement($driver.By.css(productSearchCSSPath)).sendKeys(productSearchText).then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("clicking add on first product");
  return $browser.findElement($driver.By.xpath(firstItemXPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("clicking view cart button");
  //return $browser.findElement($driver.By.partialLinkText(viewBagText)).click().then(function(){
  // ugh... y u do this?
  return $browser.get(cartURL).then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("clicking share this bag button");
  return $browser.findElement($driver.By.css(shareBagCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log('copying the cart load and then navigating to it - Page Load 6')
  return $browser.switchTo().activeElement().then(function(){
    return $browser.get($browser.findElement($driver.By.css(linkToShareCSSPath)).getAttribute("href")).then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  })
}).then(function(){
  log('going back to the desinger cart URL')
  return $browser.get(cartURL).then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log("removing items from bag");
  return $browser.findElement($driver.By.css(emptyCartCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log('closing browser now')
  $browser.close()
})

