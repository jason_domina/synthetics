require('new-relic-synthetics-manager');
//----- Add synthetic code below this line

var assert = require('assert');
var browserPageLoadInMS = 3000;
var browserElementLoadInMS = 500;

// setting live URL here...
var homePageURL = "https://new-www.stelladot.com";
// however, we can pass in a different URL to test against any other environment
//if(process.argv.indexOf("-u") != -1){homePageURL = process.argv[process.argv.indexOf("-u") + 1]}

var foundTheHomePage = "JEWELRY";
var jewelryLinkCSSPath = '#header > div > div.nav-middle > div > div.menu-wrapper > ul > li:nth-child(3) > a'
var charmsLinkCSSPath = '#header > div > div.nav-middle > div > div.menu-wrapper > ul > li:nth-child(3) > div > div > div > div.col-xs-4 > a:nth-child(5)'
var titleCSSPath = '#catalog-header > div > div > div > div:nth-child(1) > h1'
var foundTitleText = 'CHARMS & ENGRAVABLES'
var firstItemText = 'Signature Engravable ID Tag - Rose Gold';
var addToBagCSSPath = '#view_cart';
var leaveBlankGoToBagCSSPath = '#generic-modal-container > div > div > div > div.engraving-footer > button'
var goToBagCSSPath = '#nav-bag'
var cartCSSPath = '#nav-bag > span.qty';
var removeFromCartCSSPath = "#cart-items-list > div > div > div.product-column.col-sm-7 > div.cart-item-desc > div > div.clearfix.top-10 > div > a.js-remove.edit-remove-link";
var shoppingBagIsEmptyXPath = "/html/body/div[2]/div/div[1]/div[2]/div[3]/div/div/section/div[1]/div[1]/span";


var assert = require('assert'),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  lastMsg = '',
  browser = $browser.manage(),
  By = $driver.By,
  thisStep = 0
  VARS = {};
  
// enhanced logging with timestamps.  This also pushes the timestamps into Insights
var log = function(msg) {
   if (thisStep > 0) {
     var lastStep = thisStep - 1;
     var lastStepTimeElapsed = Date.now() - (startTime + stepStartTime);
     console.log('Step ' + lastStep + ': ' + lastMsg + ' FINISHED. It took ' + lastStepTimeElapsed + 'ms to complete.');
     /*
     if(process.argv.indexOf("-l") != -1){
      //we're running in local mode and don't have access to the insights toolset;
      } else {
        $util.insights.set('Stella_Stylist_A2C_Step ' + lastStep + ': ' + lastMsg, lastStepTimeElapsed);
      }
      */
   }
   stepStartTime = Date.now() - startTime;
   console.log('Step ' + thisStep + ': ' + msg + ' STARTED at ' + stepStartTime + 'ms.');
   lastMsg = msg;
   ++thisStep;
 };



$browser.get(homePageURL).then(function(){
  log("going to homepage - Page Load 1")
  return $browser.findElement($driver.By.tagName("html")).getText().then(function(body){
    log("asserting that we found evidence that we made it")
    assert.ok(body.indexOf(foundTheHomePage) != -1,"Text " + foundTheHomePage + "not found in body")
  })
}).then(function(){
  log('going to mouseover Jewlery Link')
  return $browser.findElement($driver.By.css(jewelryLinkCSSPath)).then(function(element){
    return $browser.actions().mouseMove(element).perform().then(function(){
      log('clicking the Charms & Engraveables Link - Page Load 2')
      return $browser.findElement($driver.By.css(charmsLinkCSSPath)).click().then(function(){
        $browser.sleep(browserPageLoadInMS)
      })
    })
  })
}).then(function(){
  log('before we do anything, lets make sure this is the necklaces page')
  return $browser.findElement($driver.By.css(titleCSSPath)).then(function(element){
    return element.getText().then(function(text){
      log('asserting that this is the CHARMS & ENGRAVABLES page')
      assert.equal(foundTitleText, text, 'CHARMS & ENGRAVABLES text did not match: ' + text);
    })
  })
}).then(function(){
  log('finding the first item and clicking it - Page Load 3')
  return $browser.findElement($driver.By.partialLinkText(firstItemText)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log('adding item to bag')
  return $browser.findElement($driver.By.css(addToBagCSSPath)).click().then(function(){
    return $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  log('choosing to not Customize')
  return $browser.sleep(browserPageLoadInMS).then(function(){
    return $browser.switchTo().activeElement().then(function(){
      return $browser.findElement($driver.By.css(leaveBlankGoToBagCSSPath)).click().then(function(){
        return $browser.sleep(browserPageLoadInMS)
      })
    })
  })
}).then(function(){
  log('going to Bag - Page Load 4')
  return $browser.switchTo().activeElement().then(function(){
    return $browser.sleep(browserElementLoadInMS).then(function(){
      return $browser.findElement($driver.By.css(goToBagCSSPath)).click().then(function(){
      })
    })
  })  
}).then(function(){
  log('removing item from bag')
  return $browser.sleep(browserPageLoadInMS).then(function(){
    return $browser.findElement($driver.By.css(removeFromCartCSSPath)).click().then(function(){
      return $browser.sleep(browserElementLoadInMS)
    })
  })
}).then(function(){
  log('closing browser now')
  $browser.close()
})

