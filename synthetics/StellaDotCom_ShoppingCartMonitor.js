require('new-relic-synthetics-manager');
//----- Add synthetic code below this line

var browserPageLoadInMS = 3000;
var browserElementLoadInMS = 500;

// setting live URL here...
var homePageURL = "http://www.stelladot.com";
// however, we can pass in a different URL to test against any other environment
//if(process.argv.indexOf("-u") != -1){homePageURL = process.argv[process.argv.indexOf("-u") + 1]}

var foundTheHomePage = "JEWELRY";
var couponCode = "SDEMP2017"
var jewelryLinkCSSPath = '#header > div > div.nav-middle > div > div.menu-wrapper > ul > li:nth-child(3) > a'
var necklacesLinkCSSPath = '#header > div > div.nav-middle > div > div.menu-wrapper > ul > li:nth-child(3) > div > div > div > div.col-xs-4 > a:nth-child(1)'
var necklacesCSSPath = '#catalog-header > div > div > div > div > h1'
var foundNecklaces = 'necklaces'
var firstNeckaceCSSPath = '#styles > div > div:nth-child(2) > div.row > div:nth-child(1) > div > a';
var necklaceText = 'Prisma Statement Necklace';
var addToBagCSSPath = "#view_cart";
var goToBagCSSPath = '#nav-bag > span.qty'
var discountFieldXPath = "//div[@class='cart__sidebar']/div/div[2]/form/div/div/input";
var applyDiscountButtonXPath = "//div[@class='cart__sidebar']//span[.='Apply']";
var assert = require('assert');
var discountCodeCSSPath = 'body > div.root-view.ng-scope > div > div.app__main > div.app__content > div.app__content__body.ng-scope > div > div > section > div.cart__sidebar > div > div.cart-totals__summary > uib-accordion > div > div > div.panel-collapse.in.collapse > div > div > div > div.cart-totals__row__label.ng-binding'
var foundDiscountText = 'Stella & Dot Employee Discount Code'
var clearPromoCodesXPath = "/html/body/div[2]/div/div[1]/div[2]/div[3]/div/div/section/div[2]/div/div[2]/uib-accordion/div/div/div[2]/div/div/a"
var removeFromCartCSSPath = "#cart-items-list > div > div > div.product-column.col-sm-8 > div.cart-item-desc > div > div.clearfix.top-10 > div > a.js-remove.edit-remove-link";
var shoppingBagIsEmptyCSSPath = "#cart-content > div > div > div";



$browser.get(homePageURL).then(function(){
  console.log("0.) Going to homepage - Page Load 1")
  return $browser.findElement($driver.By.tagName("html")).getText().then(function(body){
    console.log("asserting that we found evidence that we made it")
    assert.ok(body.indexOf(foundTheHomePage) != -1,"Text " + foundTheHomePage + "not found in body")
  })
}).then(function(){
  console.log('1.) going to mouseover Jewlery Link')
  return $browser.findElement($driver.By.css(jewelryLinkCSSPath)).then(function(element){
    return $browser.actions().mouseMove(element).perform().then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  }).then(function(){
      console.log('2.) Clicking the Necklaces Link - Page Load 2')
      return $browser.findElement($driver.By.css(necklacesLinkCSSPath)).click().then(function(){
        $browser.sleep(browserPageLoadInMS)
      })
    })
  }).then(function(){
  console.log('before we do anything, lets make sure this is the necklaces page')
  return $browser.waitForElement($driver.By.css(necklacesCSSPath)).then(function(element){
    return element.getText().then(function(text){
      console.log('3.) asserting that this is the necklaces page')
      assert.equal(foundNecklaces, text, 'NECKLACES text did not match: ' + text);
      return $browser.sleep(browserPageLoadInMS)
    })
  })
}).then(function(){
  console.log('4.) finding the first item and clicking it - Page Load 3')
  return $browser.findElement($driver.By.partialLinkText(necklaceText)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  console.log('5.) adding item to bag')
  return $browser.findElement($driver.By.css(addToBagCSSPath)).click().then(function(){
    return $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  console.log('6.) viewing bag - Page Load 4')
  return $browser.waitForElement($driver.By.css(goToBagCSSPath)).then(function(){
    return $browser.findElement($driver.By.css(goToBagCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  })
})
/* 
.then(function(){
  console.log('7.) clearing coupon code field')
  return $browser.findElement($driver.By.xpath(discountFieldXPath)).clear().then(function(){
    console.log('8.) typing the coupon code in')
    return $browser.findElement($driver.By.xpath(discountFieldXPath)).sendKeys(couponCode).then(function(){
      console.log('9.) clicking the button')
      return $browser.findElement($driver.By.xpath(applyDiscountButtonXPath)).click()
    })
  })
}).then(function(){
  return $browser.waitForElement($driver.By.css(discountCodeCSSPath)).then(function(element){
    return element.getText().then(function(text){
      console.log('10.) asserting that a discount code has been applied')
      assert.equal(foundDiscountText, text, 'Dicount text did not match: ' + text);
    })
  })
}).then(function(){
  console.log('11.) clearing the promo code')
  return $browser.waitForElement($driver.By.xpath(clearPromoCodesXPath)).then(function(){
    return $browser.findElement($driver.By.xpath(clearPromoCodesXPath)).click().then(function(){
      return $browser.sleep(browserElementLoadInMS)
    })
  })
})
*/
.then(function(){
  console.log('12.) removing item from cart')
  return $browser.waitForElement($driver.By.css(removeFromCartCSSPath)).then(function(){
    return $browser.sleep(browserElementLoadInMS).then(function(){
      return $browser.findElement($driver.By.css(removeFromCartCSSPath)).click().then(function(){
        return $browser.sleep(browserElementLoadInMS).then(function(){
          return $browser.waitForElement($driver.By.css(shoppingBagIsEmptyCSSPath))
        })
      })
    })
  })  
}).then(function(){
  console.log('closing browser now')
  $browser.close()
})