require('new-relic-synthetics-manager');
//----- Add synthetic code below this line
var assert = require('assert');
var browserPageLoadInMS = 3000;
var browserElementLoadInMS = 500;

//home page
var homePageURL = 'https://www.stelladot.com';
var titleSpanCSS = '#header > div > div.nav-middle > div > div.menu-wrapper > ul > li:nth-child(9) > a';
var foundTheHomePage = 'TRUNK SHOWS';

// login
var accountLoginURL = 'http://www.stelladot.com/shop/account/login'
var signInCSSPath = '#signin_submit'
var foundSignIn = 'SIGN IN'

// sale
var newSaleURL = 'https://www.stelladot.com/shop/en_us/new/sale'
var saleCSSPath = 'body > div.root-view.ng-scope > div > div.app__main > div.app__content > div.app__content__body.ng-scope > section > div.category__top > header > div > div > div > div > div:nth-child(1) > h1'
var foundSale = 'UP TO 60% OFF'

// necklaces
var necklacesURL = 'https://www.stelladot.com/shop/en_us/jewelry/necklaces/necklaces-all'
var necklacesCSSPath = '#catalog-header > div > div > div > div > h1'
var foundNecklaces = 'necklaces'

// lounge login
var loungeLoginURL = 'https://lounge.stelladot.com/'
var loungeSignInCSSPath = '#signin_submit'
var foundLoungeSignIn = 'SIGN IN'

// best sellers
var bestSellersURL = 'https://www.stelladot.com/shop/en_us/collections/best-sellers'
var bestSellerCSSPath = '#catalog-header > div > div > div > div > h1'
var foundBestSellers = 'bestsellers'

// trend report
var trendReportURL = 'https://www.stelladot.com/shop/en_us/collections/trend-report/trend-report-5'
var trendCSSPath = '#catalog-header > div > div > div > div > h1'
var foundTrend = 'collections'

// jewelry
var jewelryURL = 'https://www.stelladot.com/shop/en_us/jewelry/shop-all'
var jewleryCSSPath = '#catalog-header > div > div > div > div:nth-child(1) > h1'
var foundJewlery = 'jewelry'

// new arrivals
var newArrivalsURL = 'https://www.stelladot.com/shop/en_us/new/new-arrivals'
var newArrivalsCSSPath = '#catalog-header > div > div > div > div:nth-child(1) > h1'
var foundNewArrivals = 'new arrivals'




$browser.get(homePageURL).then(function(){
  console.log('0.) going to the homepage - Page Load 1')
  return $browser.waitForElement($driver.By.css(titleSpanCSS)).then(function(element){
    return element.getText().then(function(text){
      console.log('1.) asserting that we made it')
      assert.equal(foundTheHomePage, text, 'Homepage title did not match' + text);
    })
  })
}).then(function(){
  $browser.get(accountLoginURL).then(function(){
    console.log('2.) going to login page - Page Load 2')
    return $browser.waitForElement($driver.By.css(signInCSSPath)).then(function(element){
      return element.getText().then(function(text){
        console.log('3.) asserting that this is the sign in page')
        assert.equal(foundSignIn, text, 'Sign in text did not match: ' + text);
      })
    })
  })
})
/*
.then(function(){
  $browser.get(newSaleURL).then(function(){
    console.log('4.) going to new sale page - Page Load 3')
    return $browser.waitForElement($driver.By.css(saleCSSPath)).then(function(element){
      return element.getText().then(function(text){
        console.log('5.) asserting that this is the sale page')
        assert.equal(foundSale, text, 'Sale text did not match: ' + text);
      })
    })
  })
})
*/
.then(function(){
  $browser.get(necklacesURL).then(function(){
    console.log('6.) going to necklaces page - Page Load 4')
    return $browser.waitForElement($driver.By.css(necklacesCSSPath)).then(function(element){
      return element.getText().then(function(text){
        console.log('7.) asserting that this is the necklaces page')
        assert.equal(foundNecklaces, text, 'NECKLACES text did not match: ' + text);
      })
    })
  })
}).then(function(){
  $browser.get(loungeLoginURL).then(function(){
    console.log('8.) going to lounge login page - Page Load 5')
    return $browser.waitForElement($driver.By.css(loungeSignInCSSPath)).then(function(element){
      return element.getText().then(function(text){
        console.log('9.) asserting that this is the lounge login page')
        assert.equal(foundLoungeSignIn, text, 'SIGN IN text did not match: ' + text);
      })
    })
  })
}).then(function(){
  $browser.get(bestSellersURL).then(function(){
    console.log('10.) going to best sellers page - Page Load 6')
    return $browser.waitForElement($driver.By.css(bestSellerCSSPath)).then(function(element){
      return element.getText().then(function(text){
        console.log('11.) asserting that this is the best sellers page')
        assert.equal(foundBestSellers, text, 'Best Sellers text did not match: ' + text);
      })
    })
  })
}).then(function(){
  $browser.get(trendReportURL).then(function(){
    console.log('12.) going to trend report page - Page Load 7')
    return $browser.waitForElement($driver.By.css(trendCSSPath)).then(function(element){
      return element.getText().then(function(text){
        console.log('13.) asserting that this is the trend report page')
        assert.equal(foundTrend, text, 'TREND REPORT text did not match: ' + text);
      })
    })
  })
}).then(function(){
  $browser.get(jewelryURL).then(function(){
    console.log('14.) going to jewelry page - Page Load 8')
    return $browser.waitForElement($driver.By.css(jewleryCSSPath)).then(function(element){
      return element.getText().then(function(text){
        console.log('15.) asserting that this is the jewelry page')
        assert.equal(foundJewlery, text, 'JEWELRY text did not match: ' + text);
      })
    })
  })
}).then(function(){
  $browser.get(newArrivalsURL).then(function(){
    console.log('16.) going to new arrivals page - Page Load 9')
    return $browser.waitForElement($driver.By.css(newArrivalsCSSPath)).then(function(element){
      return element.getText().then(function(text){
        console.log('17.) asserting that this is the new arrivals page')
        assert.equal(foundNewArrivals, text, 'NEW ARRIVALS text did not match: ' + text);
      })
    })
  })
}).then(function(){
  console.log('closing browser now')
  $browser.close()
})