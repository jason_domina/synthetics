require('new-relic-synthetics-manager');
//----- Add synthetic code below this line


var browserPageLoadInMS = 3000;
var browserElementLoadInMS = 1000;
var browserInitialPageLoadInMS = 100;
var count = 0;
var maxAttempts = 5;
var browserWaitOnRetry = 1000;

// setting live URL here...
var homePageURL = 'https://lounge.stelladot.com';
// however, we can pass in a different URL to test against any other environment
//if(process.argv.indexOf("-u") != -1){homePageURL = process.argv[process.argv.indexOf("-u") + 1]}

var foundTheHomePage = 'SIGN IN';
var userEmailCSSPath = '#login-field';
var passwordCSSPath = '#password-field';
var userEmail = 'synthetics@stelladot.com';
var password = '111111';
var signinButtonCSSPath = '#signin_submit';
var ordersShopCSSPath = '#main-nav > ul > li:nth-child(2) > a'
var shopForMyselfLinkCSSPath = '#main-nav > ul > li:nth-child(2) > ul > li:nth-child(4) > a';
var searchForProductCSSPath = '#box > div > div > input'
var opportunityGuideText = 'jewelry'
var addToBagCSSPath = '#search_result > div > table > tbody > tr > div > div.hidden-xs > div.content > div > div:nth-child(5) > button';
var viewBagCSSPath = '#user-nav > ul > li.nav-node-item.bag > a';
var checkoutButtonCSSPath = '#cart > div > div.row.summary_block > div:nth-child(1) > button';
var cartItemCSSPath = '#eligible-items > div > div > table > tbody > tr > td:nth-child(1) > div > div.cart-item-desc > div:nth-child(1)'
var cartItemText = 'Travel Jewelry Box- Arrow - Travel Jewelry Box- Arrow'
var backToBagCSSPath = '#cart > div > div.row.secondary-font > div.col-xs-6.border-right.text-right > a'
var deleteLinkCSSPath = '#non-eligible-items > div > div > table > tbody > tr > td:nth-child(1) > div > div.cart-item-desc > a'
var bagStatusXPath = '//*[@id="content"]/div/div/div'
var emptyBagText = 'You have no items in your cart.'
var myAccountCSSPath ='#user-nav > ul > li.nav-node-item.dropdown > a';
var signOutText = 'Sign Out';
var signInXPath = '//*[@id="app__header__nav__item__signin"]/span/span';
var pdpURLPath = 'https://lounge.stelladot.com/shop/en_us/p/business-supplies/business-supplies/opportunity-brochure-10pack-2017-usca'
var taxValueCSSPath = '#calculated-taxes > span'

var assert = require('assert'),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  lastMsg = '',
  browser = $browser.manage(),
  By = $driver.By,
  thisStep = 0
  VARS = {};

// enhanced logging with timestamps.  This also pushes the timestamps into Insights
var log = function(msg) {
   if (thisStep > 0) {
     var lastStep = thisStep - 1;
     var lastStepTimeElapsed = Date.now() - (startTime + stepStartTime);
     console.log('Step ' + lastStep + ': ' + lastMsg + ' FINISHED. It took ' + lastStepTimeElapsed + 'ms to complete.');
     /*
     if(process.argv.indexOf("-l") != -1){
      //we're running in local mode and don't have access to the insights toolset;
      } else {
        $util.insights.set('Stella_Stylist_A2C_Step ' + lastStep + ': ' + lastMsg, lastStepTimeElapsed);
      }
      */
   }
   stepStartTime = Date.now() - startTime;
   console.log('Step ' + thisStep + ': ' + msg + ' STARTED at ' + stepStartTime + 'ms.');
   lastMsg = msg;
   ++thisStep;
 };


/*
Our site has a concurrency problem.  Since all carts are routed through the same API, the site *assumes* that there is only 
a single instance of cart possible for a user.  This test is currently setup to run across 7 locations ever 5 minutes.  It's highly likely
that we'll have 2 tests running at the same time.  Since the test adds an item the cart, proceeds to checkout, then comes back to the cart 
and removes the item, its quite possible to break the concurrently running test's cart routine.  If that happens, I've added this function
which in conjunction with handleCheckoutError, waits a configurable amount of time (var browserWaitOnRetry), and then runs this function.
Since we've already navigated to the Item's PDP through browser interaction and validated that interaction earlier in the test, I'm 
just going directly to the PDP of the item (var pdpURLPath) here in this test.  The idea is that we're waiting for the other concurrent test 
to complete its work and then repopulate the cart here so that the checkout validation can continue.
*/

 function retryAddToBag (count){
  if(count >= maxAttempts){
    log('too many tries at checkout/add to cart.  going to crash now')
    $browser.quit() 
  }
  else{
    log(count + " there was an error in the checkout, going to PDP")
    log("mousing over Order & Shop")
    return $browser.findElement($driver.By.css(ordersShopCSSPath)).then(function(element){
      return $browser.actions().mouseMove(element).perform().then(function(){
        log("Clicking the Shop for Myself Link - Page Load 2")
        return $browser.findElement($driver.By.css(shopForMyselfLinkCSSPath)).click().then(function(){
          $browser.sleep(browserPageLoadInMS)
        })
      })
  }).then(function(){
    log("Searching for Opportunity");
    return $browser.findElement($driver.By.css(searchForProductCSSPath)).sendKeys(opportunityGuideText).then(function(){
      $browser.sleep(browserPageLoadInMS)
    })
  }).then(function(){
    log("clicking the Add To Cart Button - Page Load 4")
    return $browser.findElement($driver.By.css(addToBagCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS + 5000)
    })
  }).then(function(){
    log("viewing bag - Page Load 6");
    return $browser.findElement($driver.By.css(viewBagCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  })
  }
}

function addToCart(){
  log("entering username");
  return $browser.findElement($driver.By.css(userEmailCSSPath)).sendKeys(userEmail).then(function(){
    return $browser.sleep(browserElementLoadInMS)
  }).then(function(){
    log("entering password");
    return $browser.findElement($driver.By.css(passwordCSSPath)).sendKeys(password).then(function(){
      $browser.sleep(browserElementLoadInMS)
    })
  }).then(function(){
    log("submitting form - Page Load 1");
    return $browser.findElement($driver.By.css(signinButtonCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  }).then(function(){
    log("mousing over Order & Shop")
    return $browser.findElement($driver.By.css(ordersShopCSSPath)).then(function(element){
      return $browser.actions().mouseMove(element).perform().then(function(){
        log("Clicking the Shop for Myself Link - Page Load 2")
        return $browser.findElement($driver.By.css(shopForMyselfLinkCSSPath)).click().then(function(){
          $browser.sleep(browserPageLoadInMS)
        })
      })
    })
  }).then(function(){
    log("Searching for Opportunity");
    return $browser.findElement($driver.By.css(searchForProductCSSPath)).sendKeys(opportunityGuideText).then(function(){
      $browser.sleep(browserPageLoadInMS)
    })
  }).then(function(){
    log("clicking the Add To Cart Button - Page Load 4")
    return $browser.findElement($driver.By.css(addToBagCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  }).then(function(){
    log("viewing bag - Page Load 6");
    return $browser.findElement($driver.By.css(viewBagCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  }).then(function(){
    log("asserting that our Item is in the cart")
    return $browser.findElement($driver.By.css(cartItemCSSPath)).then(function(element){
      return element.getText().then(function(text){
        assert.equal(cartItemText, text, 'cart item text did not match: ' + text);
      })
    })
  })
}

function handleCheckoutError(err){
  log("waiting 40 seconds for things to clear up because of an error: " + err.name)
  return $browser.sleep(browserWaitOnRetry).then(function(){
    log("incrementing count -> " + count)
    ++count
    return retryAddToBag(count)
    log("trying checkout again.  Attempt number: " + count)
    return checkout()
  })
}

function checkout(){
  log("clicking checkout button - Page Load 7");
  return $browser.findElement($driver.By.css(checkoutButtonCSSPath)).click().then(null,function (err) {
    if (err.name === "NoSuchElementError")
      return handleCheckoutError(err)
  }).then(function(){
    return $browser.sleep(browserPageLoadInMS)
  }).then(function(){
  
  log("clicking back to bag button - Page Load 8");
  return $browser.sleep(browserPageLoadInMS).then(function(){
    return $browser.findElement($driver.By.css(backToBagCSSPath)).click().then(null,function (err){
        return handleCheckoutError(err)
      }).then(function(){
        return $browser.sleep(browserPageLoadInMS)
      })
    })
  }).then(function(){
  log("clicking delete Link - Page Load 9");
  return $browser.findElement($driver.By.partialLinkText("Remove")).click().then(null,function (err) {
    if (err.name === "NoSuchElementError")
      return handleCheckoutError(err)
    }).then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  })
}



$browser.get(homePageURL).then(function(){
  log("Asserting that we're on the home page")
  return $browser.sleep(browserInitialPageLoadInMS).then(function(){
      $browser.findElement($driver.By.tagName("html")).getText().then(function(body){
        return assert.ok(body.indexOf(foundTheHomePage) != -1,"Text " + foundTheHomePage + " not found in body");
      })
    })
 }).then(function(){
  log("calling add to cart");
  return addToCart()
}).then(function(){
  log("running checkout function")
  return checkout()
}).then(function(){
  return signOut()
})

function signOut(){
  log("signing out of our account")
  return $browser.findElement($driver.By.css(myAccountCSSPath)).then(function(element){
    log("mousing over QA TESTER")
    return $browser.actions().mouseMove(element).perform().then(function(element){
      log("finding the sign out link and clicking it")
      return $browser.findElement($driver.By.partialLinkText(signOutText)).click().then(null,function (err) {
        if (err.name === "NoSuchElementError")
          clickPartialLinkNoSuchElementError(signOutText)
        }).then(function(){
          return $browser.sleep(browserPageLoadInMS)
        })
    })
  }).then(function(){
    log("closing browser now")
    $browser.close()
  })
}
// added a very specific 'patch' for one item that seems to fail more often than it should.  
// really, this should be generalized and used in all the findElement cases we have
function clickPartialLinkNoSuchElementError(linkText){
  return $browser.sleep(browserElementLoadInMS).then(function(){
    return $browser.findElement($driver.By.partialLinkText(linkText)).click()
  })
}