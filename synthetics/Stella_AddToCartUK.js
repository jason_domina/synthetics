require('new-relic-synthetics-manager');
//----- Add synthetic code below this line

var assert = require('assert');
var browserPageLoadInMS = 3000;
var browserElementLoadInMS = 500;

// setting live URL here...
var homePageURL = "http://www.stelladot.co.uk";
// however, we can pass in a different URL to test against any other environment
//if(process.argv.indexOf("-u") != -1){homePageURL = process.argv[process.argv.indexOf("-u") + 1]}

var foundTheHomePage = "JEWELLERY";
var couponCode = "SDEMP2017 6756565"
var jewelryLinkCSSPath = '#header > div > div.nav-middle > div > div.menu-wrapper > ul > li:nth-child(2) > a'
var necklacesLinkCSSPath = '#header > div > div.nav-middle > div > div.menu-wrapper > ul > li:nth-child(2) > div > div > div > div.col-xs-4 > a:nth-child(2)'
var necklacesCSSPath = '#sd_catalog_category_header > header > h1 > span'
var foundNecklaces = 'NECKLACES'
var firstSearchResultCSSPath = "#category_product_grid > li:nth-child(1) > a";
//var addToBagXPath = '//*[@id="add_to_bag_form_options"]/div[4]/button/div';
var addToBagXPath = '//*[@id="add_to_bag_form_options"]/div[3]/button/div'
var discountFieldXPath = "//div[@class='cart__sidebar']/div/div[2]/form/div/div/input";
var bagCSSPath = '#link-index-c'
var checkoutText = 'CHECKOUT'
var removeFromCartXPath = "//div[@class='cart__table-item__actions']//span[.='Remove']";
var shoppingBagIsEmptyXPath = "/html/body/div[2]/div/div[1]/div[2]/div[3]/div/div/section/div[1]/div[1]/span";
var proceedToCheckoutCSSPath = '#cart-header > div > a.sd-button.with-arrow'
var backToBagCSSPath = '#checkout-cart-button'
var removeItemText = 'Remove'

var assert = require('assert'),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  lastMsg = '',
  browser = $browser.manage(),
  By = $driver.By,
  thisStep = 0
  VARS = {};

// enhanced logging with timestamps.  This also pushes the timestamps into Insights
var log = function(msg) {
   if (thisStep > 0) {
     var lastStep = thisStep - 1;
     var lastStepTimeElapsed = Date.now() - (startTime + stepStartTime);
     console.log('Step ' + lastStep + ': ' + lastMsg + ' FINISHED. It took ' + lastStepTimeElapsed + 'ms to complete.');
     /*
     if(process.argv.indexOf("-l") != -1){
      //we're running in local mode and don't have access to the insights toolset;
      } else {
        $util.insights.set('Stella_A2C_UK_Step ' + lastStep + ': ' + lastMsg, lastStepTimeElapsed);
      }
      */
   }
   stepStartTime = Date.now() - startTime;
   console.log('Step ' + thisStep + ': ' + msg + ' STARTED at ' + stepStartTime + 'ms.');
   lastMsg = msg;
   ++thisStep;
 };

$browser.get(homePageURL).then(function(){
  log("going to homepage - Page Load 1")
  return $browser.findElement($driver.By.tagName("html")).getText().then(function(body){
    log("asserting that we found evidence that we made it")
    assert.ok(body.indexOf(foundTheHomePage) != -1,"Text " + foundTheHomePage + "not found in body")
  })
}).then(function(){
  log('going to mouseover Jewlery Link')
  return $browser.findElement($driver.By.css(jewelryLinkCSSPath)).then(function(element){
    return $browser.actions().mouseMove(element).perform().then(function(){
      log('clicking the Necklaces Link - Page Load 2')
      return $browser.findElement($driver.By.css(necklacesLinkCSSPath)).click().then(function(){
        $browser.sleep(browserPageLoadInMS)
      })
    })
  })
}).then(function(){
  return $browser.findElement($driver.By.css(necklacesCSSPath)).then(function(element){
    return element.getText().then(function(text){
      log('asserting that this is the necklaces page')
      assert.equal(foundNecklaces, text, 'NECKLACES text did not match: ' + text);
    })
  })
}).then(function(){
  log('finding the first item and clicking it - Page Load 3')
  return $browser.findElement($driver.By.css(firstSearchResultCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log('adding item to bag')
  return $browser.findElement($driver.By.xpath(addToBagXPath)).click().then(function(){
    return $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  log('clicking checkout in pop up dialog - Page Load 4')
  return $browser.sleep(browserPageLoadInMS).then(function(){
    return $browser.switchTo().activeElement().then(function(){
      $browser.findElement($driver.By.partialLinkText(checkoutText)).click().then(function(){
        return $browser.sleep(browserPageLoadInMS)
      })
    })
  })
}).then(function(){
  log('proceeding to checkout - Page Load 5')
  return $browser.waitForElement($driver.By.css(proceedToCheckoutCSSPath)).then(function(){
    return $browser.findElement($driver.By.css(proceedToCheckoutCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  })
}).then(function(){
  log('going back to bag - Page Load 6')
  return $browser.waitForElement($driver.By.css(backToBagCSSPath)).then(function(){
    return $browser.findElement($driver.By.css(backToBagCSSPath)).click().then(function(){
      return $browser.sleep(browserPageLoadInMS)
    })
  })
}).then(function(){
  log('removing item from cart - Page Load 7')
  return $browser.waitForElement($driver.By.partialLinkText(removeItemText)).then(function(){
    return $browser.findElement($driver.By.partialLinkText(removeItemText)).click()
  })
}).then(function(){
  log('closing browser now')
  $browser.close()
})
