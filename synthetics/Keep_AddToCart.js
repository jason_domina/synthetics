require('new-relic-synthetics-manager');
//----- Add synthetic code below this line


// setting live URL here...
var homePageURL = 'https://www.keepcollective.com';
// however, we can pass in a different URL to test against any other environment
//if(process.argv.indexOf("-u") != -1){homePageURL = process.argv[process.argv.indexOf("-u") + 1]}

var assert = require('assert');
var browserInitialPageLoadInMS = 500;
var browserPageLoadInMS = 3000;
var browserElementLoadInMS = 500;
var titleSpanCSS = '#header > div > div.nav-middle > div > div.actions-wrapper > a > span';
var foundTheHomePage = 'KEEP Collective';
var topNavStellaLinkCSSPath = '#header > div > div.nav-top > div > div.find-designer-or-hostess-region > div > div > div.stella-n-ever > a.brand-link.brand-snd.hidden-xs.hidden-sm';
var keepersAndCharmsNavCSSPath = '#nav_shop > span > span';
var braceletsText = 'Bracelets'
var firstItemXPath = '//*[@id="styles"]/div/div[2]/div[2]/div[1]/div/a'
//var firstItemCSSPath = '#styles > div > div:nth-child(3) > div.row > div:nth-child(1) > div > a > figure > figcaption > div.title'
var firstItemCSSPath = 'div#styles div:nth-child(8) > div > a > figure > figcaption > div.title'
var addToBagXPath = '//*[@id="view_cart"]'
var viewBagCSSPath = '#nav-bag'
var checkoutButtonCSSPath = '#checkout > div > div:nth-child(1) > button'
var cancelOrderLinkText = 'Cancel Order'
var modalDialogYesCSSPath = '#cancel_order_confirm'

var assert = require('assert'),
  startTime = Date.now(),
  stepStartTime = Date.now(),
  lastMsg = '',
  browser = $browser.manage(),
  By = $driver.By,
  thisStep = 0,
  VARS = {};

// enhanced logging with timestamps.  This also pushes the timestamps into Insights
var log = function(msg) {
   if (thisStep > 0) {
     var lastStep = thisStep - 1;
     var lastStepTimeElapsed = Date.now() - (startTime + stepStartTime);
     console.log('Step ' + lastStep + ': ' + lastMsg + ' FINISHED. It took ' + lastStepTimeElapsed + 'ms to complete.');
     /*
     if(process.argv[2] == '-l'){
      //we're running in local mode and don't have access to the insights toolset;
      } else {
        $util.insights.set('Keep_A2C_Step ' + lastStep + ': ' + lastMsg, lastStepTimeElapsed);
      }
      */
   }
   stepStartTime = Date.now() - startTime;
   console.log('Step ' + thisStep + ': ' + msg + ' STARTED at ' + stepStartTime + 'ms.');
   lastMsg = msg;
   ++thisStep;
 };

$browser.get(homePageURL).then(function(){
  log('going to the homepage - Page Load 1')
  return $browser.sleep(browserInitialPageLoadInMS).then(function(){
      return $browser.findElement($driver.By.css(titleSpanCSS)).then(function(element){
        return element.getText().then(function(text){
        log('asserting that we found evidence that we made it')
        assert.equal(foundTheHomePage, text, 'Homepage title did not match' + text);
      })
    })
  }) 
}).then(function(){
  log('making sure we have a link back to SnD')
  return $browser.findElement($driver.By.css(topNavStellaLinkCSSPath)).then(function(element){
    return element.getAttribute('href').then(function(link){
      assert.equal('https://www.stelladot.com/', link, 'Stella Homepage link did not match ' + link);
    })
  })
}).then(function(){
  log('going to mouseover shop keepers and charms')
  return $browser.findElement($driver.By.css(keepersAndCharmsNavCSSPath)).then(function(element){
    return $browser.actions().mouseMove(element).perform().then(function(){
      log('clicking the whats new category - Page Load 2')
      return $browser.sleep(browserElementLoadInMS).then(function(){
        return $browser.findElement($driver.By.partialLinkText(braceletsText)).click().then(function(){
          $browser.sleep(browserPageLoadInMS)
        })
      })
    })
  })
}).then(function(){
  log('clicking the first item - Page Load 3')
  return $browser.findElement($driver.By.css(firstItemCSSPath)).click().then(function(){
    $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log('clicking Add to Bag button')
  return $browser.findElement($driver.By.xpath(addToBagXPath)).click().then(function(){
    $browser.sleep(browserElementLoadInMS)
  })
}).then(function(){
  log('clicking the Bag link in top nav - Page Load 4')
  return $browser.findElement($driver.By.css(viewBagCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log('clicking the Checkout Button - Page Load 5')
  return $browser.findElement($driver.By.css(checkoutButtonCSSPath)).click().then(function(){
    return $browser.sleep(browserPageLoadInMS)
  })
}).then(function(){
  log('clicking the Cancel Order Link')
  return $browser.findElement($driver.By.partialLinkText(cancelOrderLinkText)).click().then(function(){
    $browser.sleep(browserElementLoadInMS)
  }).then(function(){
    log('switching to modal dialog box and clicking Yes - Page Load 6')
    return $browser.switchTo().activeElement().then(function(){
      $browser.findElement($driver.By.css(modalDialogYesCSSPath)).click().then(function(){
        return $browser.sleep(browserPageLoadInMS)
      })
    })
  })
}).then(function(){
  log('closing browser now')
  $browser.close()
})